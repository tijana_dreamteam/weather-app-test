# Wheater Application

### About

Application for tracking weather data in 5 cities. App tracks live temperature, wind, and forecast for selected cities using [Open Weather Map API](https://openweathermap.org/api).

### Workflow

Application is initialized with Angular CLI:

`ng new weather`

After that are created components and services. In this case that was:

```
ng g component home
ng g service city
```

Also are created models for **City** and **Forecast**.

City service we created earlier is important for communication with API. In this project are used 2 types of Open Weather Map API:

- Current Weather Data - used for live weather data for selected city
- One Call API - used for forecast temperature in the next hours

Application on the main page shows live temperature, daily minimum and maximum temperature, city name, current time, and wind speed. On bottom arrow click app requests forecast temperature data for selected city and when app get data, shows temperature forecast for next 6 hours. Then on arrow on the bottom of the city container user can collapse and hide forecast data for that city.

### Testing

Application can be tested using `ng serve` command which will run app on `localhost:4200`

### Used packages

Along default packages which are coming with angular, additional used packages are:

- Bootstrap
- Fontawesome