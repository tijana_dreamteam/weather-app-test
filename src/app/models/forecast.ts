export class Forecast {
    public temp: number;
    public dt: string;
    public dt2: Date;

    constructor(forecast) {
        this.temp = forecast.temp;
        this.dt2 = new Date(forecast.dt * 1000);
        this.dt = (this.dt2.getHours() < 10 ? '0' : '') + this.dt2.getHours() + ":" + (this.dt2.getMinutes() < 10 ? '0' : '') + this.dt2.getMinutes();
    }

}