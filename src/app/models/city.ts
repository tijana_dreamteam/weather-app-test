import { Forecast } from "./forecast";

export class City {
    public name: string;
    public temp: number;
    public temp_min: number;
    public temp_max: number;
    public icon: string;
    public description: string;
    public wind: number;
    public dt: string;
    public dt2: Date;
    public lon: number;
    public lat: number;
    public forecast: Forecast[];
    public expanded: boolean;

    constructor(city) {
        this.name = city.name;
        this.temp = city.main.temp;
        this.temp_min = city.main.temp_min;
        this.temp_max = city.main.temp_max;
        this.icon = city.weather[0].icon;
        this.description = city.weather[0].description;
        this.wind = city.wind.speed;
        this.dt2 = new Date(city.dt * 1000);
        this.dt = (this.dt2.getHours() < 10 ? '0': '') + this.dt2.getHours() + ":" + (this.dt2.getMinutes() < 10 ? '0': '') + this.dt2.getMinutes();
        this.lon = city.coord.lon;
        this.lat = city.coord.lat;
        this.forecast = [];
        this.expanded = false;
    }
}