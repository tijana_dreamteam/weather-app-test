import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { CityService } from 'src/app/services/city.service';
import { City } from '../../models/city';
import { SEARCH_URL } from '../../helpers/routes';
import { FORECAST_URL } from '../../helpers/routes';
import { Capabilities } from 'selenium-webdriver';
import { Forecast } from 'src/app/models/forecast';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title = 'Weather';
  city: String;
  cities: City[] = [];
  city_list: String[] = ['Belgrade', 'Madrid', 'Athens', 'Dubai', 'Moscow'];
  expanded: boolean;
  lat: number;
  lon: number;
  hours: 6;

  constructor(private cityService: CityService, private router: Router, private http: HttpClient) { }

  ngOnInit(): void {
    this.city_list.forEach(city => {
      this.expanded = false;
      this.cityService.search(city)
        .subscribe(res => {
          const new_city = new City(res);
          this.cities.push(new_city);
        })
    })
  }

  getForecast(city) {
    this.cityService.getForecast(city)
      .subscribe(res => {
        res.hourly = res.hourly.slice(0, 6);
        res.hourly = res.hourly.map(item => new Forecast(item));
        city.forecast = res.hourly;
        city.expanded = true;
      })
  }
}
