import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { FORECAST_URL, SEARCH_URL } from '../helpers/routes';

@Injectable()
export class CityService {

    constructor(private http: HttpClient) {

    }

    search(city): Observable<any> {
        return this.http.get(`${SEARCH_URL}&q=${city}`);
    }

    getForecast(city): Observable<any> {
        return this.http.get(`${FORECAST_URL}&lat=${city.lat}&lon=${city.lon}`);
    }
}